import 'package:flutter/material.dart';
import 'login/loginPage.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/signup': (BuildContext context) => new LoginPage()
      },
      theme: ThemeData(primaryColor: Color(0xFF1CAF9A), fontFamily: "Ubuntu"),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        //padding:EdgeInsets.only(top: 50.0, right: 20.0, left: 20.0, bottom: 20.0),
        padding: EdgeInsets.symmetric(horizontal: 15),
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
        child: ListView(
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "BIENVENIDO",
              textAlign: TextAlign.center,
              style: TextStyle(
                  height: 04.5,
                  fontSize: 50.0,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColor),
            ), 
            Image.asset(
              "assets/images/IconoLogo.png",
              height: 230,
            ),
            Text(
              "PARKING-LOJA",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 45.0,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColor),
            ),
            SizedBox(
              height: 80.0,
            ),
            buildButtonContainer(),
            
          ],
        ),
      ),
    );
  }

  Widget buildButtonContainer() {
    return GestureDetector(
      onTap: () {
        print("Container clicked");
        Navigator.of(context).pushNamed('/signup');
      },
      child: Container(
        height: 56.0,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(23.0),
          gradient: LinearGradient(
              colors: [Color(0xFF1CAF9A), Color(0xFF1CAF9B)],
              begin: Alignment.centerRight,
              end: Alignment.centerLeft),
        ),
        child: Center(
          child: Text(
            "Acceder",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
            ),
            
          ),
        ),
        
      ),
    );
  }
}
